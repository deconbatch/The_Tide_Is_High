/**
 * 
 * The Tide Is High.
 * 
 * Processing 3.2.1
 * @author @deconbatch
 * @version 0.1
 * created 0.1 2019.12.29
 */

void setup() {
  size(720, 380);
  colorMode(HSB, 360, 100, 100, 100);
  blendMode(SCREEN);
	rectMode(CENTER);
  noStroke();
  noLoop();
}

void draw() {

  int   frmMax  = 24 * 12; // for 24fps x 12s animation
  int   lineMax = 50;
  int   speed   = 3;
  float rectDiv = 0.15;
  float rectWid = 10.0;
  float baseHue = random(360.0);
  
  translate(0.0, height * 0.5);
  for (int frmCnt = 0; frmCnt < frmMax; ++frmCnt) {

    float frmRatio = map(frmCnt, 0, frmMax, 0.0, 1.0);
    background(baseHue, 100.0, 40.0, 100.0);

    for (int lineCnt = 0; lineCnt < lineMax; lineCnt++) {

	    float lineRatio = map(lineCnt, 0, lineMax, 0.0, 1.0);
      float easeRatio = easeInOutCubic((frmRatio + lineRatio) % 1.0); // add a difference to each line.

	    for (float rad = 0; rad < TWO_PI; rad += rectDiv) {
        float radRatio = map(rad, 0.0, TWO_PI, 0.0, 1.0);
        float lNoise = noise(lineRatio, radRatio);
        float fNoise = noise(sin(PI * frmRatio));
        float eNoise = noise(sin(PI * easeRatio));

        float rectX = width * radRatio;
        float rectW = (0.2 + lineRatio) * rectWid;
        float waveShape = 0.4 * height * sin(rad + TWO_PI * speed * frmRatio + PI * lNoise);
        float rectY = waveShape * sin(PI * radRatio) * eNoise;
        float rectH = 20.0 + abs(waveShape) * fNoise;
        float rectHue = baseHue + 360.0 + sin(TWO_PI * (radRatio + easeRatio)) * 30.0;

        fill(rectHue % 360.0, 90.0, 10.0, 100.0);
        rect(rectX, rectY, rectW, rectH);
	    }
    }
    saveFrame("frames/" + String.format("%04d", frmCnt) + ".png");
  }
  exit();
}

/**
 * easeInOutCubic easing function.
 * @param  t     0.0 - 1.0 : linear value.
 * @return float 0.0 - 1.0 : eased value.
 */
private float easeInOutCubic(float t) {
  t *= 2.0;
  if (t < 1.0) {
    return pow(t, 3) / 2.0;
  }
  t -= 2.0;
  return (pow(t, 3) + 2.0) / 2.0;
}
